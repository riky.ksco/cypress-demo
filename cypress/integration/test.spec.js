import { ContactUsPage } from "../support/contactUs.js";
import {
  validLogin,
  emptyLogin,
  RegisterAccount,
  errorMessageesRegistrationAccount,
} from "../fixtures/data.json";
const {
  uniqueNamesGenerator,
  adjectives,
  colors,
  animals,
} = require("unique-names-generator");
const randomName = uniqueNamesGenerator({
  dictionaries: [adjectives, animals, colors],
  length: 2,
});
const ssn = Math.floor(Math.random() * 1000000000);

const contactUsInfo = new ContactUsPage();
describe(" Registration Scenarios", () => {
  beforeEach(() => {
    contactUsInfo.visitRegistrationPage();
  });
  it("New user should be able to register new account after fillling all the required fields", () => {
    contactUsInfo.RegisterAccount(
      randomName,
      randomName,
      randomName,
      RegisterAccount.city,
      RegisterAccount.state,
      RegisterAccount.zipCode,
      RegisterAccount.PN,
      ssn,
      randomName,
      randomName,
      randomName,
      "Your account was created successfully. You are now logged in."
    );
    let myUniqueId = randomName;
    cy.task("setMyUniqueId", myUniqueId);
  });

  it("New user should be able to register new account if the username already exist", () => {
    cy.task("getMyUniqueId").then((myUniqueId) => {
      contactUsInfo.RegisterAccount(
        randomName,
        randomName,
        randomName,
        RegisterAccount.city,
        RegisterAccount.state,
        RegisterAccount.zipCode,
        RegisterAccount.PN,
        ssn,
        myUniqueId, //RegisterAccount.userNameExistent,
        randomName,
        randomName,
        "This username already exists."
      );
    });
  });

  it("New user should be able to not register new account if required fields are not filled yet", () => {
    contactUsInfo.VerifyErrorMessages(errorMessageesRegistrationAccount);
  });
});

describe.skip("Login Scenarios", () => {
  //the web page is inconsistent and sometimes get temporary unavailable, if you want to check the following test just unskip them
  beforeEach(() => {
    contactUsInfo.visitMainPage();
  });
  it("submit correct information", () => {
    contactUsInfo.Login(
      validLogin.user,
      validLogin.Password,
      validLogin.Assertion,
      validLogin.loggedInPage
    );
  });

  it("submit empty information", () => {
    contactUsInfo.tryLoginwithoutTypeCredentials(
      emptyLogin.Assertion,
      emptyLogin.loggedInPage
    );
  });
});
