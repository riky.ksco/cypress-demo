export class ContactUsPage {
  user = () => cy.get('[name="username"]');
  passwordLogin = () => cy.get('[name="password"]');
  loginButton = () => cy.get('[value="Log In"]');
  firstname = () => cy.get('[id="customer.firstName"]');
  lastName = () => cy.get('[id="customer.lastName"]');
  address = () => cy.get('[id="customer.address.street"]');
  city = () => cy.get('[id="customer.address.city"]');
  state = () => cy.get('[id="customer.address.state"]');
  zipcode = () => cy.get('[id="customer.address.zipCode"]');
  PN = () => cy.get('[id="customer.phoneNumber"]');
  SSN = () => cy.get('[id="customer.ssn"]');
  userName = () => cy.get('[id="customer.username"]');
  passwordRegistration = () => cy.get('[id="customer.password"]');
  confirm = () => cy.get('[id="repeatedPassword"]');
  registerButton = () => cy.get('input[value="Register"]');

  RegisterAccount = (
    firstName,
    lastName,
    address,
    city,
    state,
    zipcode,
    PN,
    SSN,
    userName,
    password,
    confirm,
    message
  ) => {
    this.firstname().should("be.visible").type(firstName);
    this.lastName().should("be.visible").type(lastName);
    this.address().should("be.visible").type(address);
    this.city().should("be.visible").type(city);
    this.state().should("be.visible").type(state);
    this.zipcode().should("be.visible").type(zipcode);
    this.PN().should("be.visible").type(PN);
    this.SSN().should("be.visible").type(SSN);
    this.userName().should("be.visible").type(userName);
    this.passwordRegistration().should("be.visible").type(password);
    this.confirm().should("be.visible").type(confirm);
    this.registerButton().should("be.visible").click();
    cy.findAllByText(`${message}`).should("be.visible");
  };
  accountCreatedAssertion = () => {};
  Login = (user, password, assertion, path) => {
    this.user().should("be.visible").type(user);
    this.passwordLogin().should("be.visible").type(password);
    this.loginButton().should("be.visible").click();
    cy.findAllByText(assertion).should("be.visible");
    cy.url().should("include", path);
  };

  triggerErrorMessages = () => {
    this.registerButton().should("be.visible").click();
    cy.findAllByText(assertion).should("be.visible");
  };
  VerifyErrorMessages = (errorMessageArray) => {
    this.registerButton().should("be.visible").click();
    for (let i = 0; i < errorMessageArray.length; i++) {
      cy.findAllByText(errorMessageArray[i].message);
    }
  };
  tryLoginwithoutTypeCredentials = (assertion, path) => {
    this.loginButton().should("be.visible").click();
    cy.findAllByText(assertion).should("be.visible");
    cy.url().should("include", path);
  };

  visitMainPage = () => {
    return cy.visit("https://parabank.parasoft.com/parabank/index.htm");
  };
  visitRegistrationPage = () => {
    return cy.visit("https://parabank.parasoft.com/parabank/register.htm");
  };
  ChechElementisDetached = (cssSelector) => {
    cy.get(cssSelector, { timeout: 20000 }).should(($el) => {
      expect(Cypress.dom.isDetached($el)).to.eq(false);
    });
  };
}
