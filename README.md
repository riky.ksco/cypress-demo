# Cypress-demo


This is demo  app used to showcase [Cypress.io](https://www.cypress.io/) testing. this app run the following test cases on this page https://parabank.parasoft.com/parabank/index.htm

### 1. Run the tests 

```bash
## clone this repo to a local directory
git clone https://gitlab.com/riky.ksco/cypress-demo

## cd into the cloned repo
cd cypress-demo
## install the node_modules
npm install

## cypress runs the tests locally  
npm run cy:run

### 2. Install & write tests in Cypress

[Follow these instructions to install and write tests in Cypress.](https://on.cypress.io/installing-cypress)

### 3. How the test looks running with the server locally
I set up a **.gitlab-ci.yml** file that include the configuration to run headless after any changes to check if the test works correctly integrated with **image: cypress/browsers:node11.13.0-chrome73**